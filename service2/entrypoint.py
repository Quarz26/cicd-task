import requests
import sys
import os

SERVICE1_URL = os.environ['URL']

inp = sys.stdin.readline().strip()

if len(inp) == 0:
    print("No argument was given.")
    exit(0)

message = requests.get(inp).text
data = ["md5", message]

print(requests.post(SERVICE1_URL, data="\n".join(data).encode('utf-8')).text)