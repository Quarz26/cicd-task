#!/bin/bash

POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
    --version_service1)
      VERSION_SERVICE1="$2"
      shift
      shift
      ;;
    --version_service2)
      VERSION_SERVICE2="$2"
      shift
      shift
      ;;
    --image_service1)
     IMAGE_SERVICE1="$2"
     shift
     shift
     ;;
    --image_service2)
     IMAGE_SERVICE2="$2"
     shift
     shift
     ;;
    *)
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

docker rm -f service1 &>/dev/null && echo 'Removed old container for Service 1'
docker rm -f service2 &>/dev/null && echo 'Removed old container for Service 2'
docker network rm avl &>/dev/null && echo 'Removed avl network'

docker network create avl &>/dev/null && echo "Created avl network"
docker run --network avl -d --name service1 -p 8081:8080 $IMAGE_SERVICE1:$VERSION_SERVICE1 &>/dev/null && echo "Service 1 is running on http://localhost:8081"
docker run --env URL=http://service1:8080 --network avl -d --name service2 -p 8080:8080 $IMAGE_SERVICE2:$VERSION_SERVICE2 &>/dev/null && echo "Service 2 is running on http://localhost:8080"